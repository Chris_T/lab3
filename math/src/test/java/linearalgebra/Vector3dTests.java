//Christian Tran 1937391

package linearalgebra;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import linearalgebra.linearalgebra.Vector3d;

public class Vector3dTests {
    
    @Test
    public void returnsGood(){
        boolean test = true;
        double x = Math.random();
        double y = Math.random();
        double z = Math.random();

        Vector3d vec1 = new Vector3d(x,y,z);

        if (x != vec1.getX() || y != vec1.getY() || z != vec1.getZ()){
            test = false;
        }

        assertTrue(test);
    }

    @Test
    public void magnitudeTest(){
        boolean magTest = true;
        double x = Math.random();
        double y = Math.random();
        double z = Math.random();

        Vector3d vec1 = new Vector3d(x, y, z);

        if (Math.sqrt(x*x+y*y+z*z) != vec1.magnitude()){
            magTest = false;
        }

        assertTrue(magTest);
    }

    //Christian Tran 1937391
    @Test
    public void dotProductTest(){
        boolean dotTest = true;
        double x1 = Math.random();
        double y1 = Math.random();
        double z1 = Math.random();
        double x2 = Math.random();
        double y2 = Math.random();
        double z2 = Math.random();

        Vector3d vec1 = new Vector3d(x1, y1, z1);
        Vector3d vec2 = new Vector3d(x2, y2, z2);

        if (x1*x2+y1*y2+z1*z2 != vec1.dotProduct(vec2)){
            dotTest = false;
        }

        assertTrue(dotTest);
    }

    @Test
    public void addTest(){
        boolean sumTest = true;
        double x1 = Math.random();
        double y1 = Math.random();
        double z1 = Math.random();
        double x2 = Math.random();
        double y2 = Math.random();
        double z2 = Math.random();

        Vector3d vec1 = new Vector3d(x1, y1, z1);
        Vector3d vec2 = new Vector3d(x2, y2, z2);
        Vector3d sumVec = vec1.add(vec2);

        if (x1+x2 != sumVec.getX() || y1+y2 != sumVec.getY() || z1+z2 != sumVec.getZ()){
            sumTest = false;
        }

        assertTrue(sumTest);
    }
}
