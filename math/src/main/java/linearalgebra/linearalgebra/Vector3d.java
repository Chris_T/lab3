//Christian Tran 1937391

package linearalgebra.linearalgebra;

public class Vector3d {

    private double x;
    public double getX() {
        return x;
    }

    private double y;
    public double getY() {
        return y;
    }
    private double z;
    public double getZ() {
        return z;
    }

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public double magnitude(){
        return Math.sqrt(getX()*getX()+getY()*getY()+getZ()*getZ());
    }

    public double dotProduct(Vector3d dotVec){
        return (getX()*dotVec.getX() + getY()*dotVec.getY() + getZ()*dotVec.getZ());
    }

    public Vector3d add(Vector3d sumVec){
        Vector3d newVector3d = new Vector3d(getX() + sumVec.getX(), getY() + sumVec.getY(), getZ() + sumVec.getZ());
        return newVector3d;
    }

    public String toString(){
        return ("x: " + getX() + " y: " + getY() + " z: " + getZ());
    }
}